import os, sys
from pydub import AudioSegment
from pathlib import Path
#INFO ON PYDUB: https://github.com/jiaaro/pydub#dependencies
#DEPENDENCY FFMPEG LINK: http://www.ffmpeg.org/download.html
#HOW TO INSTALL FFMPEG ONCE YOU HAVE THE UNCOMPRESSED FOLDER: http://blog.gregzaal.com/how-to-install-ffmpeg-on-windows/

def obtain_correct_order_of_files(source_path):
    files = []
    with open(source_path, 'r') as file:
        for line in file:
            if line != '\n':
                files.append(line[:len(line)-1]) #THIS REMOVES '\n'
    return files

def obtain_files_in_disorder(target_path):
    files = []
    if os.path.exists(target_path):
        files = os.listdir(target_path)
    else:
        print(target_path + ' doesn\'t exist!')
    return files

def create_paths(files, target_path):
    paths = []
    for item in files:
        paths.append(target_path + item)
    return paths

def create_complete_file(parameters):
    audio_segments = []
    one_second_silence = AudioSegment.silent(duration=1000)
    final_file_path_element = 'Conversation {}'.format(parameters['unit'] + '.mp3')
    final_file = parameters['target_path'] + final_file_path_element

    print('Creating the following file: ' + final_file_path_element)
    for item in parameters['target_paths']:
        audio_segments.append(AudioSegment.from_mp3(item))
        print('Creating audio segment from: ' + item)
        os.remove(item)
        print('Removed file in: ' + item)

    print('\nCreating complete audio file in: ' + final_file)

    for item in audio_segments[1:]:
        audio_segments[0] += one_second_silence + item

    final_file_path = Path(final_file)

    if not final_file_path.exists():
        audio_segments[0].export(final_file, format = 'mp3')
        print('Created file: ' + final_file_path_element + '!')
    else:
        print(final_file + ' already exists!')

source_path = 'C:/Users/Xerxes/Desktop/web_scraping_scripts/correct_order_of_files.txt'
target_path = 'C:/Users/Xerxes/Desktop/{}/'
unit = sys.argv[1]
target_path = target_path.format(unit)

files_in_order = []
files_in_order = obtain_correct_order_of_files(source_path)
target_paths = create_paths(files_in_order, target_path)
parameters = {'target_path':target_path, 'unit':unit, 'target_paths':target_paths}
create_complete_file(parameters)
