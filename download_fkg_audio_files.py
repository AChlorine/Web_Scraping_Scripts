import urllib3, logging, os, sys, shutil
from pathlib import Path

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def setup_logger():
    logging.basicConfig(level=logging.DEBUG)
    return logging.getLogger(__name__)

def create_request(url):
    try:
        http = urllib3.PoolManager()
        request = http.request('GET', url)
    except Exception as e:
        logger.exception(e)
        request = ' '
    return request

def download_files(logger, audio_files, parameters):
    errors = []
    print('Amount of files to download: ' + str(len(audio_files)))

    for index, file in enumerate(audio_files):
        try:
            response = create_request(parameters['root_path'].format(parameters['unit'], file))
            target_path = parameters['target_path'].format(parameters['unit'])
            target_path = os.path.join(target_path, file)
            with open(target_path, 'wb') as file_to_download:
                file_to_download.write(response.data)
            print(str(index) + '. Downloaded: ' + parameters['unit'] + ', ' + file)
        except Exception as e:
            logger.exception(e)
            errors.append(file)
            pass
    if errors:
        print('The following files could not be extracted: ')
        for index, item in enumerate(errors):
            print(str(index) + '. ' + errors)
        delete_folders(parameters['target_path'])
    print('All operations complete!')

def create_folder(parameters):
    if("%20" in parameters['unit']):
        parameters['unit'] = parameters['unit'].replace('%20', " ")
    target_path = parameters['target_path'].format(parameters['unit'])
    if not os.path.exists(target_path):
        os.makedirs(target_path)
        print(target_path + ' was created successfully!')
    else:
        print(target_path + ' already exists!')
def fix_names_with_spaces(name):
    return name.replace(" ", "%20")
def delete_folders(path):
    print('Deleting: ' + path)
    shutil.rmtree(path)
    print(path + ' has been deleted!')

audio_files = ['introduction1.mp3', 'gachaget.mp3', 'battlestart1.mp3', 'generalvoice1.mp3', 'battlestart2.mp3', 'normalattack1.mp3', 'normalattack2.mp3', 'generalvoice3.mp3', 'skillattack1.mp3', 'skillattack2.mp3', 'generalvoice2.mp3', 'damage1.mp3', 'criticaldamage1.mp3', 'dead1.mp3', 'generalvoice4.mp3', 'victory1.mp3', 'narrowvictory1.mp3', 'bootmypagetalk1.mp3', 'easyvictory1.mp3', 'existence1.mp3', 'existence2.mp3', 'bootmypagetalk2.mp3', 'defeat1.mp3', 'defeat2.mp3', 'bootmypagetalk3.mp3', 'finddoor1.mp3', 'findout1.mp3', 'enjoyment1.mp3', 'ptselect1.mp3', 'ptselect2.mp3', 'equipchange1.mp3', 'enjoyment2.mp3', 'charalevelup1.mp3', 'evolut1.mp3', 'freegachaplay.mp3', 'mypagetalk1.mp3', 'enjoyment3.mp3', 'mypagetalk2.mp3', 'staminamax.mp3', 'mypagetalk3.mp3', 'mypageignore.mp3', 'present1.mp3', 'present2.mp3', 'stagestart1.mp3', 'stagestart2.mp3', 'title1.mp3', 'loginbonus.mp3']

root_path = 'https://harem-battle.club/audio/fkg/units/{}/{}'
target_path = 'C:/Users/Xerxes/Desktop/{}/'
unit = sys.argv[1]
unit = fix_names_with_spaces(unit)

parameters = {'root_path':root_path, 'target_path':target_path, 'unit':unit}

logger = setup_logger()
create_folder(parameters)
download_files(logger, audio_files, parameters)
