import urllib3, urllib
from bs4 import BeautifulSoup

def obtain_words(amount):
    words = []
    for i in range(0, amount):
        word = str(input(str(i + 1) + ". "))
        words.append(word.lower())
    return words

def create_urls(url, words):
    urls = []
    special_chars = ['á', 'é', 'í', 'ó', 'ú']
    for word in words:
        word = urllib.parse.quote(word.encode('utf-8')) #ENCODE RETURNS STRING WITH /X. YOU NEED TO REPLACE THIS /X WUTH % TO GET CORRECT RESULT IN URL 
        urls.append(url.format(word))
    return urls

def obtain_requests_from_site(urls):
    requests = []
    for url in urls:
        requests.append(create_request(url))
    return requests

def create_request(url):
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    return request

def obtain_data_from_site(requests):
    data = []
    for request in requests:
        items = []
        if request.status == 200:
            print("Request status: " + str(request.status))
            soup = BeautifulSoup(request.data, 'html.parser')
            div = soup.findAll('div', {'class':'ds-single'})
            for item in div:
                items.append(item.text)
            data.append(items)
        else:
            print('Request failed')
            data.append(items)
    print()
    return data

def print_data(data, words):
    for index, word in enumerate(words):
        if data[index]:
            spacing = len(word) + 10
            print('#'*spacing)
            print('### ' + str(index + 1) + '.' + word + ' ###')
            print('#'*spacing)
            print()
            for definitions in data[index]:
                print(definitions)
            print()

def obtain_amount():
    is_number = False
    while not is_number:
        try:
            amount = int(input("Amount of words to search: "))
            is_number = True
        except:
            print("Not a number!")
    return amount
url = 'es.thefreedictionary.com/{}'
amount = obtain_amount()
words = obtain_words(amount)
urls = create_urls(url, words)
requests = obtain_requests_from_site(urls)
data = obtain_data_from_site(requests)
print_data(data, words)
