import urllib3, pandas, numpy
from bs4 import BeautifulSoup
from IPython.display import display
from copy import deepcopy

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

### PRINTING FUNCTIONS ###
def print_whole_tables(lists, headers):
    print('\nPrinting table')
    dataframe = load_dataframe_with_lists(lists, headers)
    display(dataframe.to_string(index=False))

def print_all_names(lists):
    print('\nPrinting all names')
    names = pandas.DataFrame(columns = ['Index', 'Name'])
    names['Index'] = lists['Index']
    names['Name'] = lists['T-Doll']
    display(names.to_string(index=False))

def obtain_class():
    classes = ['HG', 'AR', 'SMG', 'RF', 'SG', 'MG']
    good_class = False
    while not good_class:
        given_class = input('Class: ')
        if given_class in classes:
            return given_class
        else:
            print('Invalid class!')
def obtain_class_indices(classes, type):
    indices = []
    for index, item in enumerate(classes):
        if item == type:
            indices.append(index)
    return indices

def prep_lists_with_indices(lists, indices_raw):
    #YOU'RE MAKING A COPY OF LISTS, THEN DELETING THEIR CONTENT, SO YOU CAN ADD ONLY THE INFORMATION OF SPECIFIC INDICES IN INDICES RAW
    lists2 = deepcopy(lists)
    for item in lists2:
        lists2[item] = []
    for item in lists:
        for index in indices_raw:
            lists2[item].append(lists[item][index])
    return lists2


def print_by_class(lists, headers):
    print('\nClasses: HG, AR, SMG, SG, RF, MG')
    given_class = obtain_class()
    indices = obtain_class_indices(lists['Class'], given_class)
    lists2 = prep_lists_with_indices(lists, indices)
    print_whole_tables(lists2, headers)

def print_comparison(lists, headers):
    dataframe = load_dataframe_with_lists(lists, headers)

    indices = obtain_tdoll_indices(lists)

    headers2 = ['',
                dataframe['T-Doll'][indices['index1']],
                ' ', #NEEDS TO BE THIS WAY, OTHERWISE, CONTENTS OF FIRST HEADER IS PLACED INTO THIS HEADER AS WELL
                dataframe['T-Doll'][indices['index2']]]

    tdolls = pandas.DataFrame(columns = headers2)

    t_doll_1_data = obtain_data_from_dataframe(dataframe, headers, indices['index1'])
    t_doll_1_data = turn_str_data_into_int(t_doll_1_data)
    t_doll_2_data = obtain_data_from_dataframe(dataframe, headers, indices['index2'])
    t_doll_2_data = turn_str_data_into_int(t_doll_2_data)
    comparison = compare_stats(t_doll_1_data, t_doll_2_data, headers2)

    tdolls[headers2[0]] = headers[2:]
    tdolls[headers2[1]] = t_doll_1_data
    tdolls[headers2[2]] = comparison['results']
    tdolls[headers2[3]] = t_doll_2_data

    print()
    print('T-Doll Stat Comparison')
    display(tdolls)
    print('\n' + comparison['verdict'] + '\n')

### DATA PREP ###

def remove_line_jump(items):
    for index, item in enumerate(items):
        items[index] = item.replace('\n', '')
    return items

def turn_str_data_into_int(data):
    for index, item in enumerate(data[1:]): #FIRST ELEMENT IS UNIT CLASS
        data[index + 1] = int(item)
    return data

def prepare_lists(data):
    indices = []
    names = []
    classes = []
    damage = []
    evasion = []
    accuracy = []
    rof = []
    hp = []
    ammo = []
    armor = []

    for index in range(0, len(data), 10):
        indices.append(data[index])
    for index in range(1, len(data), 10):
        names.append(data[index])
    for index in range(2, len(data), 10):
        classes.append(data[index])
    for index in range(3, len(data), 10):
        damage.append(data[index])
    for index in range(4, len(data), 10):
        evasion.append(data[index])
    for index in range(5, len(data), 10):
        accuracy.append(data[index])
    for index in range(6, len(data), 10):
        rof.append(data[index])
    for index in range(7, len(data), 10):
        hp.append(data[index])
    for index in range(8, len(data), 10):
        ammo.append(data[index])
    for index in range(9, len(data), 10):
        armor.append(data[index])

    lists = {'Index':indices,
            'T-Doll':names,
            'Class':classes,
            'Damage':damage,
            'Evasion':evasion,
            'Accuracy':accuracy,
            'ROF':rof,
            'HP':hp,
            'Ammo':ammo,
            'Armor':armor}
    return lists

def load_dictionary(t_dolls):
    dictionary = {}
    for index, item in enumerate(t_dolls):
        #FOR SOME REASON, IF YOU DON'T DO THIS, THERE IS SOME SPACE IN THE INDEX AND THE NAME
        dictionary[item.replace(' ', '')] = index
    return dictionary

def obtain_data_from_dataframe(dataframe, headers, index):
    #USING A DICTIONARY MAKES IT SO THAT LOOK-UPS ARE FASTER
    data = []
    for header in headers[2:]:
        if dataframe[header][index] == '':
            #SOME T-DOLLS DON'T HAVE CERTAIN STATS, THUS THEIR VALUES IN THE TABLE ARE EMPTY. THIS MAKES IT SO THE EMPTY SPACE IS TURNED INTO A 0, SO VALUES CAN BE COMPARED
            data.append(0)
        else:
            data.append(dataframe[header][index])
    return data

def load_dataframe_with_lists(lists, headers):
    dataframe = pandas.DataFrame(columns = headers)
    for header in headers:
        dataframe[header] = numpy.array(lists[header])
    return dataframe

def obtain_tdoll_indices(lists):
    print()
    dictionary = load_dictionary(lists['T-Doll'])
    print('1. ', end = '')
    index1 = return_tdoll_index(dictionary)
    print('2. ', end = '')
    index2 = return_tdoll_index(dictionary)
    indices = {'index1':index1, 'index2':index2}
    return indices

def return_tdoll_index(dictionary):
    good_name = False
    while not good_name:
        try:
            tdoll= input("Name: ")
            index = dictionary[tdoll.replace(' ', '')]
            good_name = True
        except:
            print('Invalid T-Doll Name!')
    return index

def obtain_input(type):
    types = {'select':'Selection: ', 'class':'Class: '}
    choices = [1,2,3,4]
    good_selection = False
    while not good_selection:
        try:
            choice = int(input(types[type]))
            if choice not in choices:
                print("Not a valid number!")
            else:
                good_selection = True
        except:
            print('Invalid option!')
    return choice

### LOGIC ###

def compare_stats(data1, data2, headers):
    results = []
    verdict = ''

    doll1 = 0
    doll2 = 0

    for index, item in enumerate(data1):
        if type(item) == str:
            results.append('')
        else:
            if data1[index] == data2[index]:
                results.append('=')
            elif data1[index] > data2[index]:
                results.append('>')
                doll1 += 1
            else:
                results.append('<')
                doll2 += 1

    if doll1 == doll2:
        verdict = "They both have their strong points."
    elif doll1 > doll2:
        #VALUES OF HEADERS IS THE NAME OF T-DOLLS
        verdict = headers[1] + " defeats " + headers[3]
    else:
        verdict = headers[3] + " defeats " + headers[1]
    final_results = {'results':results, 'verdict':verdict}
    return final_results

def menu(lists):
    print('Options: \n1. Display all names\n2. Display full table\n3. Display by class\n4. Comparison between two T-Dolls')
    choice = obtain_input('select')
    menu_choices(choice, lists)


def menu_choices(selection, lists):
    headers = ['Index', 'T-Doll', 'Class', 'Damage', 'Evasion', 'Accuracy', 'ROF', 'HP', 'Ammo', 'Armor']

    if selection == 1:
        print_all_names(lists)
    elif selection == 2:
        print_whole_tables(lists, headers)
    elif selection == 3:
        print_by_class(lists, headers)
    elif selection == 4:
        print_comparison(lists, headers)

### OBTAIN DATA FROM WEB ###

def create_request(url):
    http = urllib3.PoolManager()
    request = http.request('GET', url)
    return request

def obtain_data_from_site(request, path):
    items = []
    if request.status == 200:
        print("Request status: " + str(request.status))
        soup = BeautifulSoup(request.data, 'html.parser')
        table = soup.find('table', {'class':'gf-table sortable'})
        for row in table.findAll('td'):
            items.append(row.text)
        #THE ORIGINAL TABLE HAS AN EMPTY ROW. YOU CANNOT DIRECTLY MODIFY THE TABLE SINCE THE SITE IS NOT YOURS, SO AS A WORKAROUND, YOU JUST DELETE THE FIRST ROW, WHICH CONTAINS TEN ELEMENTS
        items = items[10:]
        write_data_into_file(items, path)
    else:
        print('Request failed!')
    return items

### WORKING WITH FILES ###
def write_data_into_file(items, path):
    with open(path, 'w+') as file:
        for item in items:
            file.write(item)

def read_data_from_file(path):
    data = []
    with open(path, 'r') as file:
        for lines in file:
            data.append(lines)
    return data

def do_with_internet(url, path):
    #RUN THIS WHENEVER YOU NEED TO UPDATE YOUR INFO
    request = create_request(url)
    data = obtain_data_from_site(request, path)

### MAIN ###
def main():
    path = '/home/chlorine/Desktop/Web Scraping/data.txt'
    url = 'https://en.gfwiki.com/wiki/T-Doll_List_(Maximum_Stats)'

    data = read_data_from_file(path)
    data = remove_line_jump(data)
    lists = prepare_lists(data)
    menu(lists)

if __name__ == '__main__':
    main()
